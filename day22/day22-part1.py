#!/usr/bin/env python3

import sys
from collections import defaultdict


STEPCOORD = {"R": (0, 1), "L": (0, -1), "U": (-1, 0), "D": (1, 0)}
LEFTTURN = {"R": "U", "U": "L", "L": "D", "D": "R"}
RIGHTTURN = {"R": "D", "D": "L", "L": "U", "U": "R"}
FACESCORE = {"R": 0, "D": 1, "L": 2, "U": 3}


class World:
    def __init__(self, map, instructions) -> None:
        self.grid = defaultdict(lambda: " ")
        self.position = None
        self.dir = "R"
        self.parse_grid(map)
        self.parse_instructions(instructions)

    def parse_grid(self, map):
        self.max_col = 0
        for r, row in enumerate(map.split("\n")):
            for c, char in enumerate(row):
                self.grid[(r, c)] = char
                if not self.position and char != " ":
                    self.position = (r, c)
            self.max_col = max(self.max_col, c)
        self.max_row = r

    def parse_instructions(self, instructions):
        res = []

        temp = ""
        for c in instructions:
            if c in "RL":
                if temp:
                    res.append(int(temp))
                    temp = ""
                res.append(c)
            elif c in "1234567890":
                temp += c
        if temp:
            res.append(int(temp))
        self.instructions = res

    def step(self, num):
        for _ in range(num):
            r, c = self.position
            dr, dc = STEPCOORD[self.dir]
            if self.grid[(r + dr, c + dc)] == ".":
                self.position = (r + dr, c + dc)
            elif self.grid[(r + dr, c + dc)] == "#":
                return
            elif self.grid[(r + dr, c + dc)] == " ":
                if self.dir == "R":
                    c = 0
                elif self.dir == "L":
                    c = self.max_col
                elif self.dir == "U":
                    r = self.max_row
                elif self.dir == "D":
                    r = 0
                else:
                    assert False
                while self.grid[(r, c)] == " ":
                    r, c = r + dr, c + dc
                if self.grid[(r, c)] == ".":
                    self.position = (r, c)
                elif self.grid[(r, c)] == "#":
                    return
                else:
                    assert False
            # print(self.position, self.dir)

    def turn(self, dir):
        if dir == "R":
            self.dir = RIGHTTURN[self.dir]
        elif dir == "L":
            self.dir = LEFTTURN[self.dir]
        else:
            assert False

    def run(self):

        for inst in self.instructions:

            if inst == "R" or inst == "L":
                self.turn(inst)
            else:
                self.step(inst)

        r, c = self.position
        return (r + 1) * 1000 + (c + 1) * 4 + FACESCORE[self.dir]


with open(sys.argv[1], "r") as f:
    map, instructions = f.read().strip("\n").split("\n\n")

world = World(map, instructions)

part1 = world.run()
print(f"Part 1: {part1}")

part2 = ""
print(f"Part 2: {part2}")
