# Advent of Code 2022

Solutions to the Advent of Code 2022 challenge. I'll be doing most of it in Python, but may do some Rust or Bash if the opportunity arises.

Videos for each challenge are here: https://www.youtube.com/playlist?list=PLJt6nPUdQbiTXW_lHJ6d2u8NnsyPaKGeI

`genday22.sh` is my script to quickly pull the daily input, create some folder structure, and start a stub solution script. It assumes Python unless `rust` is added as an additional argument.
