#!/usr/bin/env python3

import sys


with open(sys.argv[1], "r") as f:
    data = f.read().strip()

elves = data.split("\n\n")


largest = []
for elf in elves:
    total = sum(int(x) for x in elf.split("\n") if x)
    # if total > largest:
    #     largest = total
    largest = sorted(largest + [total], reverse=True)[:3]

print(f"Part 1: {largest[0]}")

print(f"Part 2: {sum(largest)}")


totals = sorted([sum(map(int, elf.split("\n"))) for elf in elves], reverse=True)
print(f"Part 1: {totals[0]}")
print(f"Part 2: {sum(totals[:3])}")
