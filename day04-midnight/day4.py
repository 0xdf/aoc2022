#!/usr/bin/env python3

import sys


with open(sys.argv[1], "r") as f:
    lines = f.readlines()

count = 0
for line in lines:
    e1, e2 = line.split(",", 2)
    e1 = list(map(int, e1.split("-")))
    e2 = list(map(int, e2.split("-")))
    if e1[0] <= e2[0] and e1[1] >= e2[1]:
        count += 1
        continue
    if e1[0] >= e2[0] and e1[1] <= e2[1]:
        count += 1

count2 = 0
for line in lines:
    e1, e2 = line.split(",", 2)
    e1 = list(map(int, e1.split("-")))
    e2 = list(map(int, e2.split("-")))
    if e1[1] >= e2[0] and e1[1] <= e2[1]:
        count2 += 1
        continue
    if e2[1] >= e1[0] and e2[1] <= e1[1]:
        count2 += 1

print(f"Part 1: {count}")

print(f"Part 2: {count2}")
