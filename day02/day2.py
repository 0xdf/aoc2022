#!/usr/bin/env python3

import sys

"""The score for a single round is the score for the shape you selected 
(1 for Rock, 2 for Paper, and 3 for Scissors) plus the score for the outcome of the round 
(0 if you lost, 3 if the round was a draw, and 6 if you won)."""

scores = {
    "A X": 1 + 3,  # Rock / Rock (1)         = Draw (3)
    "A Y": 2 + 6,  # Rock / Paper (2)        = Win (6)
    "A Z": 3 + 0,  # Rock / Scissors (3)     = Loss (0)
    "B X": 1 + 0,  # Paper / Rock (1)        = Loss (0)
    "B Y": 2 + 3,  # Paper / Paper (2)       = Draw (3)
    "B Z": 3 + 6,  # Paper / Scissors (3)    = Win (6)
    "C X": 1 + 6,  # Scissors / Rock (1)     = Win (6)
    "C Y": 2 + 0,  # Scissors / Paper (2)    = Loss (0)
    "C Z": 3 + 3,  # Scissors / Scissors (3) = Draw (3)
}

"""X means you need to lose, Y means you need to end the round in a draw, and Z means you need to win."""

scores2 = {
    "A X": 0 + 3,  # Rock / Loss (0)     = Scissors (3)
    "A Y": 3 + 1,  # Rock / Draw (3)     = Rock (1)
    "A Z": 6 + 2,  # Rock / Win (6)      = Paper (2)
    "B X": 0 + 1,  # Paper / Loss (0)    = Rock (1)
    "B Y": 3 + 2,  # Paper / Draw (3)    = Paper (2)
    "B Z": 6 + 3,  # Paper / Win (6)     = Scissors (3)
    "C X": 0 + 2,  # Scissors / Loss (0) = Paper (2)
    "C Y": 3 + 3,  # Scissors / Draw (3) = Scissors (3)
    "C Z": 6 + 1,  # Scissors / Win (6)  = Rock (1)
}

with open(sys.argv[1], "r") as f:
    lines = f.readlines()

part1 = sum([scores[l.strip()] for l in lines])
print(f"Part 1: {part1}")

part2 = sum([scores2[l.strip()] for l in lines])
print(f"Part 2: {part2}")
