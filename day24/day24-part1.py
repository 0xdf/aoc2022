#!/usr/bin/env python3

import sys
from collections import deque
from functools import cache


@cache
def get_storms(t):
    current = set()
    for r, row in enumerate(lines[1:-1]):
        for c, char in enumerate(row[1:-1]):
            if char in "<v>^":
                dr, dc = DIRS[char]
                current.add(((r + dr * t) % numr, (c + dc * t) % numc))
    return current


DIRS = {"<": (0, -1), "v": (1, 0), ">": (0, 1), "^": (-1, 0)}

with open(sys.argv[1], "r") as f:
    lines = f.readlines()

# parse input
numr = len(lines) - 2  # not counting walls
numc = len(lines[0]) - 3  # not counting walls
start = (-1, lines[0].index(".") - 1)
end = (numr, lines[-1].index(".") - 1)

# bfs
queue = deque()
# state = (time, me_r, me_c)
queue.append((0, *start))
seen = set()
part1 = None

while not part1:
    t, r, c = queue.popleft()
    if (t, r, c) in seen:
        continue
    seen.add((t, r, c))

    # check for moves
    storms = get_storms(t + 1)
    if (r, c) not in storms:
        queue.append((t + 1, r, c))
    for dr, dc in DIRS.values():
        # if can reach the end, do it, we're done
        if (r + dr, c + dc) == end:
            part1 = t + 1
            break
        # check for points in bounds that can be moved to
        elif 0 <= r + dr < numr and 0 <= c + dc < numc:
            if (r + dr, c + dc) not in storms:
                queue.append((t + 1, r + dr, c + dc))

print(f"Part 1: {part1}")
part2 = ""
print(f"Part 2: {part2}")
