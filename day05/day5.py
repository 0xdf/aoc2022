#!/usr/bin/env python3

import sys
from collections import defaultdict


with open(sys.argv[1], "r") as f:
    lines = f.read()

crate_raw, insts = lines.rstrip().split("\n\n")

#         [H]     [W] [B]
#     [D] [B]     [L] [G] [N]
# [P] [J] [T]     [M] [R] [D]
# [V] [F] [V]     [F] [Z] [B]     [C]
# [Z] [V] [S]     [G] [H] [C] [Q] [R]
# [W] [W] [L] [J] [B] [V] [P] [B] [Z]
# [D] [S] [M] [S] [Z] [W] [J] [T] [G]
# [T] [L] [Z] [R] [C] [Q] [V] [P] [H]
#  1   2   3   4   5   6   7   8   9

# i  row
# 1  1
# 5  2
# 9  3

crates = defaultdict(list)
crates2 = defaultdict(list)

for ci in crate_raw.split("\n")[:-1][::-1]:
    i = 1
    while i < len(ci):
        if ci[i] != " ":
            crates[(i + 3) // 4].append(ci[i])
            crates2[(i + 3) // 4].append(ci[i])
        i += 4


for inst in insts.split("\n"):
    # move 3 from 2 to 9
    _, num, _, start, _, stop = inst.split(" ")
    num, start, stop = int(num), int(start), int(stop)

    for i in range(num):
        crates[stop].append(crates[start].pop())
    crates2[stop].extend(crates2[start][-num:])
    crates2[start] = crates2[start][:-num]

part1 = "".join(c[-1] for c in crates.values())
print(f"Part 1: {part1}")

part2 = "".join(c[-1] for c in crates2.values())
print(f"Part 2: {part2}")
