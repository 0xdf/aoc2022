#!/bin/bash


day=${1##+(0)}
if ((day < 1 || day > 25)); then
    return
fi
    
project=$(printf "day%02d" $1)
if [ -z "$AOC_SESSION" ]; then
    echo "$AOC_SESSION isn't set. Cannot continue."
    exit
fi

if [ "$2" = "rust" ]; then

    cargo new ${project}-rs

    cd ${project}-rs

    curl -s "https://adventofcode.com/2022/day/${day}/input" --cookie "session=${AOC_SESSION}" -o input.txt

    echo -n 'fn main() {
    let data = include_str!("../input.txt").trim();
    println!(
        "Part 1: {}",
        ""
    );

    println!(
        "Part 2: {}",
        ""
    );
}' > src/main.rs

else

    mkdir ${project}

    cd ${project}

    curl -s "https://adventofcode.com/2022/day/${day}/input" --cookie "session=${AOC_SESSION}" -o input.txt

    echo -n "#!/usr/bin/env python3

import sys


with open(sys.argv[1], 'r') as f:
    lines = f.readlines()
    
part1 = \"\"
print(f'Part 1: {part1}')

part2 = \"\"
print(f'Part 2: {part2}')" > day${day}.py

fi